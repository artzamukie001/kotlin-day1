//fun main(args: Array<String>) {
//    gradReport("SomChai", 3.33)
//    gradReport()
//    gradReport(name = "nobita")
//    gradReport(gpa = 2.50)
//    helloMore()
//    println(isOdd(5))
//    println(getAbbreviation('A'))
//    println(getGrade(105))
//    println(getAbbreviation(105))
//}

fun helloMore(
    fname: String = "Sarawut",
    surname: String = "Boonruang"
): Unit {
    println("$fname $surname")
}

fun gradReport(
    name: String = "annonymous",
    gpa: Double = 0.0
): Unit {
    println("mister $name gpa: is $gpa")
}

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'C' -> {
            println("not smart")
            return "Cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 71..80 -> grade = "B"
        in 81..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun getAbbreviation(score: Int): String? {
    var grade = getGrade(score)
    if (grade == null) {
        return "invalid jubjub"
    } else {
        return getAbbreviation(grade.toCharArray()[0])
    }
}

fun main(args: Array<String>) {
    var arrays = arrayOf(5, 10, 100, 25, 1, 6)
    var max = findMaxValue(arrays)

    println("max value is $max")
}

fun findMaxValue(values:Array<Int>): Int {
    var max:Int=0
    for (i in values.indices) {
        if(max<=values[i]){
            max=values[i]
        }
    }
    return max
}


